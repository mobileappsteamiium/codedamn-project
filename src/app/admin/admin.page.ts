import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserService } from '../user.service'
import { firestore } from 'firebase'
import { AlertController } from '@ionic/angular'
import { ReturnStatement } from '@angular/compiler';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  courseName:string
  courseCode:string
  section:string
  day:string
  startTime:string
  endTime:string
  lecturer:string

  daylist: string[] =[
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday"
  ]

  timelist: string[] =[
    "8.30",
    "10.00",
    "11.30",
    "1.00",
    "2.00",
    "3.30",
    "5.00"
  ]

  seclist: string[] =[
    "1",
    "2",
    "3",
    "4"
  ]

  constructor(
    public router: Router,
    public afstore: AngularFirestore,
    public user: UserService,
    public alert: AlertController

    ) { }

  ngOnInit() {
    
  }

  submit(){
    this.showAlert()
  }
  
  logout(){
    this.router.navigate(['/login'])
  }
  
  customPopoverOptions1: any = {
    header: 'SELECT DAY',
    //subHeader: 'Select day of the class',
    //message: 'Select day of the class'
  };
  
  customPopoverOptions2: any = {
    header: 'SELECT TIME',
    //subHeader: 'Select day of the class',
    //message: 'Select day of the class'
  };

  customPopoverOptions3: any = {
    header: 'SELECT SECTION',
    //subHeader: 'Select day of the class',
    //message: 'Select day of the class'
  };

  async showAlert() {
    const alert = await this.alert.create({
      header: 'Confirmation',
      message: '<strong>Add Subject</strong>?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'YES',
          handler: () => {
            console.log('Confirm Okay');
            this.ssubmit()
          }
        }
      ]
    });

    await alert.present();
  }

  ssubmit(){
    
    const name= this.courseName
    const code= this.courseCode
    const section= this.section
    const day= this.day
    const startTime= this.startTime
    const endTime= this.endTime
    const lecturer= this.lecturer

    this.afstore.doc(`admin/${code}`).set({
        name,
        code,
        section,
        day,
        startTime,
        endTime,
        lecturer
    })
    
    console.log("success")
    this.courseName= null
    this.courseCode= null
    this.section=null
    this.day= null
    this.startTime= null
    this.endTime= null
    this.lecturer= null
  }
}
