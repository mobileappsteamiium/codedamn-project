import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRouteSnapshot } from '@angular/router';
import { firestore } from 'firebase';
import { AlertController} from '@ionic/angular'

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.page.html',
  styleUrls: ['./uploader.page.scss'],
})
export class UploaderPage implements OnInit {
  
  CourseCode: string = ""
  courseName:string
  courseCode:string
  section:string
  day:string
  startTime:string
  endTime:string
  lecturer:string
  subjectDetails: Boolean = false
  timeData
  mySubject
  myDay
  myTime

  constructor(
    public user: UserService, 
    public alert: AlertController,
    public afstore: AngularFirestore,
    ) { }

  ngOnInit() {
  }

  Add(){
    this.subjectDetails = false
    
    this.afstore.collection('admin', ref=> ref.where('code', '==', this.CourseCode))
    .get().toPromise().then((snapshot) => {
      snapshot.docs.forEach(doc =>{
        console.log(doc.data().name)
        this.ssubmit(doc.data())
        this.subjectDetails = true
      })
    this.CourseCode=null
    })
  }

  ssubmit(data){
    
    const name= data.name
    const code= data.code
    const section= data.section
    const day= data.day
    const startTime= data.startTime
    const endTime= data.endTime
    const lecturer= data.lecturer

    this.afstore.doc(`users/${this.user.getID()}`).update({
      subject: firestore.FieldValue.arrayUnion({
        name,
        code,
        section,
        day,
        startTime,
        endTime,
        lecturer
      })
    })
    this.showAlert("Success","Subject has been added")
    console.log("success")
    
  }

  async showAlert(header: string, message:string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })

    await alert.present()
  }

}
