import { Component, OnInit } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/auth'
import { auth } from 'firebase/app'
import { AlertController } from '@ionic/angular'
import { Router } from '@angular/router'
import { firestore } from 'firebase';

import { AngularFirestore } from '@angular/fire/firestore'
import { UserService } from '../user.service'
import { AuthService } from '../auth.service'


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  username: string = ""
  password: string = ""
  cpassword: string = ""
  
  constructor(
    public afAuth: AngularFireAuth,
    public afstore: AngularFirestore,
    public alert: AlertController,
    public router: Router,
    public user: UserService,
    ) { }

  ngOnInit() {
  }

  async register(){
    const { username, password, cpassword } = this
    if(password !== cpassword){
      this.showAlert("Error!","Passwords not match")
      return console.error("Check carefully")
    }

    try{
      const res = await this.afAuth.auth.createUserWithEmailAndPassword(username, password)

      this.afstore.doc(`users/${res.user.uid}`).set({
        username,
        subject: firestore.FieldValue.arrayUnion({})
      })

     // this.afstore.doc(`users/${res.user.uid}`).set({
     //   subject: firestore.FieldValue.arrayUnion({})
     // })
      
      this.user.setUser({
        username,
        ID: res.user.uid
      })

      console.log(res)
      console.info("Congrats")
      this.showAlert("Welcome","Your account has been created")
      this.router.navigate(['/tabs'])
    } catch (err) {
      console.dir(err)
      this.showAlert("Error!",err.message)
    }
  }

  async showAlert(header: string, message:string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["Noted"]
    })

    await alert.present()
  }

  login(){
    this.router.navigate(['./login'])
  }
}
