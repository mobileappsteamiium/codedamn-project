import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage implements OnInit {

  subjectID: string
  subject
  userSubject
  dayList: any[] = [
    "Monday",
    "Tuesday",
    "Wednesday", 
    "Thursday",
    "Friday"
  ]

  constructor(  private afs: AngularFirestore, private user: UserService,
                private route: ActivatedRoute) { 
    const subject = afs.doc(`users/${user.getID()}`)
    this.userSubject = subject.valueChanges()
  }

  ngOnInit() {
   /* this.subjectID = this.route.snapshot.paramMap.get('id')
    this.subject = this.afs.doc(`subject/${this.subjectID}`).valueChanges() */
  }

}
