import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { first } from 'rxjs/operators';


interface user{
    username: string,
    ID: string
}

@Injectable()
export class UserService {
    private user:user

    constructor( private afAuth: AngularFireAuth){

    }

    setUser(user: user){
        this.user = user
    }

    async isAuthenticated() {
        if(this.user) return true

        const user = await this.afAuth.authState.pipe(first()).toPromise()

        if(user) {
            this.setUser({
                username: user.email,
                ID: user.uid
            })

            return true
        }
        return false
    }

    getID(): string{
        return this.user.ID
    }
}