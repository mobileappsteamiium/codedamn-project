import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }
  
  URL1(){ 
    window.open('http://myapps.iium.edu.my/StudentOnline/schedule1.php', '_system'); 
  }
  
  
  URL2(){ 
    window.open('http://prereg.iium.edu.my/', '_system'); 
  }
  
  URL3(){ 
    window.open('http://www.iium.edu.my/ece/ece_academic_staffs.htm', '_system'); 
  }
  
  logout(){
    this.router.navigate(['/login'])
  }
}
